variable "tags" {
  type        = map(any)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)."
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `organization`, `name`, `environment` and `attributes`."
}

locals {
  # labels
  name        = "webdenav"
  name_prefix = module.labels.id 
  environment = "prod"

  # Network
  # availability_zone = element(data.aws_availability_zones.available.names, 0)
  vpc_id            = data.terraform_remote_state.vpc-prod.outputs.vpc_id-prod
  subnet_id         = data.terraform_remote_state.vpc-prod.outputs.subnet_id-prod
}

variable "public_in_port" {
  description   = "The port the ALB will use for HTTP requests"
  type          = number
  default       = 80
}

variable "nlb_name" {
  description   = "The name of the NLB"
  type          = string
  default       = "nlb"
}

variable "instance_security_group_name" {
  description   = "The name of the security group for the EC2 Instances"
  type          = string
  default       = "sg-instance"
}

variable "public_security_group_name" {
  description   = "The name of the security group for the NLB"
  type          = string
  default       = "sg-public"
}
