#!/bin/bash -xe
# echo "Install Ansible ..."
# exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
cd /home/ubuntu/
sudo apt update -y
sudo apt install python3-pip -y
sudo apt install ansible -y

# echo "Install Docker ..."
cd /home/ubuntu/
git clone https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/install-docker.git
cd /home/ubuntu/install-docker
ansible-playbook playbooks/pl_install_docker_local.yml

# echo "Install Fluentd ..."
cd /home/ubuntu/
git clone https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/setup-efk.git
cd /home/ubuntu/setup-efk
ansible-playbook playbooks/pl_setup_f_local.yml
